**Attention! Snakemake has moved to Github: https://github.com/snakemake/snakemake!**
Please file new issues and pull requests over there. This repository will stay a while for archiving purposes.